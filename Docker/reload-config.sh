#!/bin/bash

# Define the paths
COMMON_CONF="/home/openvpn/config/server-common.conf"
TCP_CONF="/home/openvpn/config/server-tcp.conf"
UDP_CONF="/home/openvpn/config/server-udp.conf"

# Change ownership of all files in config folder
sudo chown -R openvpn:openvpn /home/openvpn/config/

# Backup old logfiles before starting OpenVPN
if [ -f /home/openvpn/logs/openvpn.log ]; then
    cp /home/openvpn/logs/openvpn.log /home/openvpn/logs/openvpn.log.backup
fi
if [ -f /home/openvpn/config/openvpn-tcp-status ]; then
    cp /home/openvpn/logs/openvpn-tcp-status /home/openvpn/logs/openvpn-tcp-status.backup
fi
if [ -f /home/openvpn/config/openvpn-udp-status ]; then
    cp /home/openvpn/logs/openvpn-udp-status /home/openvpn/logs/openvpn-udp-status.backup
fi

# Implement iptables rules if the config file exist
if [ -f /home/openvpn/config/iptables.sh ]; then
    sudo chmod +x /home/openvpn/config/iptables.sh
    sudo /home/openvpn/config/iptables.sh
fi

# Check if the common configuration file exists
if [ -f "$COMMON_CONF" ]; then
    # If the common config exists, check for the TCP config
    if [ -f "$TCP_CONF" ]; then
        # Start a new instance of OpenVPN with the TCP config
        echo "Starting OpenVPN with TCP configuration..."
        sudo openvpn --config "$TCP_CONF" &
        # We run with sudo, because without this openvpn won't have rights to utilize /dev/net/tun
    fi

    # Check for the UDP config
    if [ -f "$UDP_CONF" ]; then
        # Start another instance of OpenVPN with the UDP config
        echo "Starting OpenVPN with UDP configuration..."
        sudo openvpn --config "$UDP_CONF" &
        # We run with sudo, because without this openvpn won't have rights to utilize /dev/net/tun
    fi

    # Now generic exporter / should read tcp & udp files together
    sudo /home/openvpn/exporter/ras_generic_exporter --config.file=/home/openvpn/ras_generic_exporter.yml &

else
    # If the common config does not exist, start tailing /dev/null
    echo "Common configuration not found. Starting process to tail -f /dev/null..."
    tail -f /dev/null
fi

# Wait for all background processes to finish
wait