# Use the official Debian base image
FROM debian:latest

# Set environment variables to avoid interactive prompts during package installation
ENV DEBIAN_FRONTEND=noninteractive

# Update package list and install required packages
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    ca-certificates \
    openvpn \
    net-tools \
    tcpdump \
    ethtool \
    iputils-ping \
    iproute2 \
    curl \
    wget \
    vim \
    nano \
    ssh \
    sudo \
    procps \
    git \
    make \
    golang \
    iptables \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Create a non-root user for running OpenVPN
RUN groupadd openvpn
RUN useradd -m -s /bin/bash openvpn -g openvpn

# Ensure /dev/net/tun exists
RUN mkdir -p /dev/net && \
    mknod /dev/net/tun c 10 200 && \
    chmod 666 /dev/net/tun

# Allow the openvpn user to execute sudo commands without password
RUN echo 'openvpn ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

# Switch to the openvpn user
USER openvpn

# Create directory for OpenVPN configuration & logs
RUN mkdir -p /home/openvpn/config
RUN mkdir -p /home/openvpn/logs

# Set the working directory
WORKDIR /home/openvpn

# Send logs from openvpn to stdout
RUN ln -sf /dev/stdout /home/openvpn/logs/openvpn.log

# Copy the reload script to the container
COPY --chown=openvpn:openvpn Docker/reload-config.sh /home/openvpn/reload-config.sh
COPY --chown=openvpn:openvpn Docker/ras_generic_exporter.yml /home/openvpn/ras_generic_exporter.yml

# Ensure the script is executable
RUN chmod +x /home/openvpn/reload-config.sh

# Run required commands for exporter
RUN git clone https://git.trollprod.org/Prom_Exporters/ras_generic_exporter --branch 2.0.3 /home/openvpn/exporter/
RUN make -C /home/openvpn/exporter/ build

# Expose OpenVPN port
EXPOSE 443/tcp
EXPOSE 443/udp
EXPOSE 9234/tcp

# Set the entrypoint to the script
ENTRYPOINT ["/home/openvpn/reload-config.sh"]