# OpenVPN Instance

This repository contains the Docker Image for the OpenVPN.

For more information:

 - the [CONTRIBUTING](./CONTRIBUTING.md) document describes how to contribute to the repository
 - in case of need, please contact owner group : [RAS Squad](mailto:DIGIT-NMS-REMOTE-ACCESS@ec.europa.eu)
 - see [Changelog](./CHANGELOG.md) for release information.



## Quick Start

* Create OpenVPN network on Docker host for our OpenVPN installation:
```
docker-host-vm# docker network create openvpn             
469e1a1117184a005ffe7336d85553dbf6478ac65fad1f1af378dde5ca7721df

docker-host-vm# docker network ls             
NETWORK ID     NAME      DRIVER    SCOPE
40ebca5614c8   bridge    bridge    local
6a9e980e04e7   host      host      local
f570617a127e   none      null      local
469e1a111718   openvpn   bridge    local # This is the created network we will use
```
* Create OpenVPN volumes on Docker host for our OpenVPN installation:
```
docker-host-vm# docker volume create openvpn_config
openvpn_config

docker-host-vm# docker volume create openvpn_logs  
openvpn_logs

docker-host-vm# docker volume ls                 
DRIVER    VOLUME NAME
local     openvpn_config  # This will be used for configuration
local     openvpn_logs    # This will be used for logs
```

* Create OpenVPN container using created elements with default port configuration
```
docker-host-vm# docker run -d --name openvpn -v openvpn_config:/home/openvpn/config -v openvpn_logs:/home/ope
nvpn/logs --network=openvpn --cap-add NET_ADMIN --device /dev/net/tun:/dev/net/tun openvpn

docker-host-vm# docker ps
CONTAINER ID   IMAGE     COMMAND                  CREATED         STATUS         PORTS                        NAMES
eecea91ea4b2   openvpn   "/home/openvpn/reloa…"   6 seconds ago   Up 5 seconds   443/tcp, 9234/tcp, 443/udp   openvpn
```

* OpenVPN container config folder is empty by default:
```
docker-host-vm# docker exec -it eecea91ea4b2 ls -lah config
total 12K
drwxr-xr-x 2 openvpn openvpn 4.0K Oct 23 14:44 .
drwxr-xr-x 1 openvpn openvpn 4.0K Oct 30 15:00 ..
```

* Copy configruation to OpenVPN container to mounted volume:
```
# Generated configuration with all related files are in folder:
docker cp <filesystem_path_with_required_files>/. openvpn:/home/openvpn/config/
```

* Restart container so our new configuration will be re-applied:
```
docker-host-vm# docker restart openvpn
```

## Debugging Tips

* Logging of OpenVPN logs are send on container to stdout and therefore we can see the logs using command:

```
docker-host-vm# docker logs -f openvpn
Common configuration not found. Starting process to tail -f /dev/null...
```

## How Does The Test Work?

Testing pipeline "DOCKER/TEST is doing following steps:

1. Login to container registry (in this repository)
```
docker login -u <username> -p <password> https://code.europa.eu:4567/digit-c4/openvpn
```
2. Pull openvpn (server) image with "latest" tag
```
docker pull https://code.europa.eu:4567/digit-c4/openvpn:latest
```
3. Create openvpn network with specific IP range (192.168.200.0/24)
```
docker network create --subnet=192.168.200.0/24 openvpn
```
4. Create two volumes
 - openvpn_logs - for logging tests
 - openvpn_config - for configuration storage
```
docker volume create openvpn_logs
docker volume create openvpn_config
```
5. Build 2 new testing container images for CA (certificate authority and config gemeration) and Client containers
```
# In case they already exist, remove the images:
docker rm -f openvpn_ca
docker rm -f openvpn_client
# Build image for CA:
docker builder build -t openvpn_ca -f Test/openvpn_ca.dockerfile .
# Build images for clients:
docker builder build -t openvpn_client_bookworm -f Test/openvpn_client_bookworm.dockerfile .
docker builder build -t openvpn_client_bullseye -f Test/openvpn_client_bullseye.dockerfile .
docker builder build -t openvpn_client_jammy -f Test/openvpn_client_jammy.dockerfile .
docker builder build -t openvpn_client_focal -f Test/openvpn_client_focal.dockerfile .
```
6. Run CA container that will stop after it will finish his job:
- it will deploy certificate authority and also generate and sign certificates for server and client
- it will generate all server and client required configuration files
```
docker run -d --name openvpn_ca -v openvpn_config:/home/openvpn/config openvpn_ca
```
7. Once container will stop, we will remove it
```
# timeout 10m docker wait openvpn_ca
docker rm openvpn_ca
```
8. Afterwards we will run both containers:
- server container: using image stored in container registry of this repository
- client container: using new image created just for testing purposes
```
# Run server container:
docker run -d --name openvpn_server -v openvpn_config:/home/openvpn/config -v openvpn_logs:/home/openvpn/logs --network=openvpn --ip=192.168.200.100 --cap-add NET_ADMIN --device /dev/net/tun:/dev/net/tun https://code.europa.eu:4567/digit-c4/openvpn:latest

# Run client containers
docker run -d --name openvpn_client_bookworm -v openvpn_config:/home/openvpn/config -v openvpn_logs:/home/openvpn/logs --network=openvpn --ip=192.168.200.10 --cap-add NET_ADMIN --device /dev/net/tun:/dev/net/tun openvpn_client_bookworm
docker run -d --name openvpn_client_bullseye -v openvpn_config:/home/openvpn/config -v openvpn_logs:/home/openvpn/logs --network=openvpn --ip=192.168.200.11 --cap-add NET_ADMIN --device /dev/net/tun:/dev/net/tun openvpn_client_bullseye
docker run -d --name openvpn_client_jammy -v openvpn_config:/home/openvpn/config -v openvpn_logs:/home/openvpn/logs --network=openvpn --ip=192.168.200.12 --cap-add NET_ADMIN --device /dev/net/tun:/dev/net/tun openvpn_client
docker run -d --name openvpn_client_focal -v openvpn_config:/home/openvpn/config -v openvpn_logs:/home/openvpn/logs --network=openvpn --ip=192.168.200.13 --cap-add NET_ADMIN --device /dev/net/tun:/dev/net/tun openvpn_client_focal
```
9. Client contianer will perform tests in his entrypoint script
- validation if VPN can be established and with it also confirmation that it listen on specific port (in our case 443)
- validation that exporter is running on service TCP/9234 and confirmation that it is responding to client requests
```
# Adding static DNS entry on client side:
sudo bash -c "echo '192.168.200.100 cicd.openvpn.com' >> /etc/hosts"
# Start client with generated configuration
echo "Starting client with generated configuration ..."
# We run with sudo, because without this openvpn won't have rights to utilize /dev/net/tun
sudo openvpn --config "$CLIENT_CONF" &
# We also check curl to see exporter statistics
curl -vvv http://cicd.openvpn.com:9234/metrics
```
10. Show docker logs of client container
- validation of docker logs is working (logging part)
```
# Sleep 30 to make sure we will have established VPN and some logs to show
docker logs openvpn_client_bookworm
docker logs openvpn_client_bullseye
docker logs openvpn_client_jammy
docker logs openvpn_client_focal

# And it will also show logs from server
docker logs openvpn_server
```
11. Stop all containers
```
# Stop server container
docker stop openvpn_server
# Stop all client containers
docker stop openvpn_client_bookworm
docker stop openvpn_client_bullseye
docker stop openvpn_client_jammy
docker stop openvpn_client_focal
```
12. Remove all containers
```
# Remove server container
docker rm openvpn_server
# Remove all client containers
docker rm openvpn_client_bookworm
docker rm openvpn_client_bullseye
docker rm openvpn_client_jammy
docker rm openvpn_client_focal
```
13. Remove all images
```
# Remove server container image
docker rmi openvpn_server
# Remove all client container images
docker rmi openvpn_client_bookworm
docker rmi openvpn_client_bullseye
docker rmi openvpn_client_jammy
docker rmi openvpn_client_focal
```
14. Remove both volumes
```
docker volume rm openvpn_config
docker volume rm openvpn_logs
```
15. Remove docker network
```
openvpn network rm openvpn
```
