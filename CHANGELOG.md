# Change Log
All notable changes to this project will be documented in this file.

### [v1.0.0] - 2024-12-11

#### Release notes

First major release after PoC phase ended.

#### Fixed

Nothing.
 
#### Added

Nothing.
   
#### Changed

Version v0.4.3 was promoted to v1.0.0 after successful PoC.

#### Known bugs

Build-in exporter only reads data from TCP server, not UDP. This will be fixed in next version.

### [v0.4.3] - 2024-10-30

#### Release notes

Exporter runner updated with sudo privileges and push to background.

### [v0.4.2] - 2024-10-30

#### Release notes

Update of exporter engine to latest stable version, fixing bug with OpenVPN status reading.

### [v0.4.1] - 2024-10-24

#### Release notes

Change of exporter engine

### [v0.4.0] - 2024-10-08

#### Release notes

IPtables feature is enabled in case config script exist in /home/openvpn/config/iptables.sh
Logs are now in different folder than config
- config folder: /home/openvpn/config/
- logs and status file/s folder: /home/openvpn/logs/

### [v0.3.4] - 2024-08-28

#### Release notes

Exporter required sudo to run, because status file belongs to root.
UDP server exporter must be setup with different port, so this was adjusted within script and also in EXPOSED config.

### [v0.3.3] - 2024-08-28

#### Release notes

Status files are excluded from stdout symlinks
They are renamed in a way they won't end with .log
Exporter config us updated based on it.

### [v0.3.2] - 2024-08-28

#### Release notes

Log files are moved back to:
```
/home/openvpn/config 
```
folder mapped to Docker volume
Backup of old logs is happening in script
Prometheus exporter is installed together with supporting sofware (golang and make).
Exporter is running for tcp and udp only when it is deployed.

### [v0.3.1] - 2024-08-23

#### Release notes

Removed unused systemd from dockerfile.

### [v0.3.0] - 2024-08-21

#### Release notes

Implemented and verified logging feature towards stdout

### [v0.2.2] - 2024-07-28

#### Release notes

Version with updated logging and tunnel interface working.

### [v0.2.1] - 2024-07-17

#### Release notes

Version where we have working script that will enable openvpn once configuration is in place.